## DETECTOR DE OBJETOS COM YOLOv3-320

Simples detector de objetos utilizando YOLOv3.

- **tutorial**: pasta com alguns textos úteis para a compreensão do algoritmo;
- **yolo**: contém os arquivos de configuração e pesos do YOLOv3;
- **requirements.txt**: dependências para rodar o algoritmo (também contém algumas dependências extras para se trabalhar com Deep Learning e Computer Vision);
- **yolo_demo**: detector de objetos em Python.
