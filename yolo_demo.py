import cv2
import numpy as np

cap = cv2.VideoCapture(0)
wh = 320  # weight, height
classes_file = 'yolo\coco.names'
classes_names = []
conf_threshold = 0.5
nms_threshold = 0.3

with open(classes_file, 'rt') as file:
    classes_names = file.read().rstrip('\n').split('\n')
# print(classes_names)

model_configuration = 'yolo\yolov3.cfg'
model_weights = 'yolo\yolov3.weights'
# model_configuration = 'yolov3-tiny.cfg'
# model_weights = 'yolov3-tiny.weights'
net = cv2.dnn.readNetFromDarknet(model_configuration, model_weights)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)


def findObjects(ouputs, frame):
    height, width, channels = frame.shape
    bbox = []
    class_ids = []
    confs = []

    for output in outputs:
        for detection in output:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]

            if confidence > conf_threshold:
                w, h = int(detection[2] * width), int(detection[3] * height)
                x, y = int(detection[0] * width - w /
                           2), int((detection[1] * height) - h/2)
                bbox.append([x, y, w, h])
                class_ids.append(class_id)
                confs.append(float(confidence))

    #  print(len(bbox))
    indices = cv2.dnn.NMSBoxes(bbox, confs, conf_threshold, nms_threshold)

    for i in indices:
        i = i[0]
        box = bbox[i]
        x, y, w, h = box[0], box[1], box[2], box[3]
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 0, 255), 2)
        cv2.putText(
            frame, f'{classes_names[class_ids[i]].upper()} {int(confs[i]*100)}%',
            (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)


while True:
    success, frame = cap.read()

    blob = cv2.dnn.blobFromImage(
        frame, 1/255, (wh, wh), [0, 0, 0], 1, crop=False)
    net.setInput(blob)

    layer_names = net.getLayerNames()
    # [0] remove os parênteses e recebe o primeiro elemento
    output_names = [layer_names[i[0]-1] for i in net.getUnconnectedOutLayers()]
    # print(output_names)
    # net.getUnconnectedOutLayers()
    outputs = net.forward(output_names)
    # print(len(outputs))
    # print(outputs[0].shape)

    findObjects(outputs, frame)

    cv2.imshow('WEBCAM', frame)

    if cv2.waitKey(1) == ord('q'):
        break


cap.release()
cv2.destroyAllWindows()
